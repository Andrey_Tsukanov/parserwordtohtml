﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using OpenXmlPowerTools;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.IO;

namespace ParserWordToHTML.BLL.Services
{
	public class ParserWordToHTMLService
	{

		#region  Основной код
		static readonly Regex regexOpenTag = new Regex(@"<[^/]\S*>");
		static readonly Regex regexСloseTag = new Regex(@"</\S*>");

		static public List<HTMLComponent> CreateListObjectHTMLComponent(byte[] byteArrayWord)
		{
			using (MemoryStream memoryStream = new MemoryStream())
			{
				memoryStream.Write(byteArrayWord, 0, byteArrayWord.Length);
				using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(memoryStream, true))
				{
					HtmlConverterSettings settings = new HtmlConverterSettings()
					{
						PageTitle = "My Page Title",
						FabricateCssClasses = false
					};

					XElement html = HtmlConverter.ConvertToHtml(wordDoc, settings);

					var elements = html.Elements();
					var bodyTag = elements.ElementAt(1);
					var divTags = bodyTag.Elements();
					var divTag = divTags.ElementAt(0);
					var pTagsList = divTag.Elements().ToList();

					#region    // Формируем строку для словаря.

					string wordToString = "";
					foreach (var tagP in pTagsList)
					{
						wordToString += (tagP.Value + " ");
					}
					#endregion

					Dictionary<string, string> dictionaryOpenTagKeyCloseTagValue = ParserLineToDictionaryTag(wordToString);
					HTMLComponent htmlComponent = new HTMLComponent();
					var index = 0;
					List<HTMLComponent> listHTMLComponents = new List<HTMLComponent>();
					ParserWordToHTMLRecursion(ref index, pTagsList, htmlComponent, listHTMLComponents, dictionaryOpenTagKeyCloseTagValue);
					return listHTMLComponents;
				}
			}
		}
		//Метод для составления списка.
		static void ParserWordToHTMLRecursion(ref int index, List<XElement> pTagsList, HTMLComponent htmlComponent, List<HTMLComponent> listHTMLComponents,
			Dictionary<string, string> dictionaryOpenTagKeyCloseTagValue)
		{
			HTMLComponent htmlComponent2 = htmlComponent;
			string targetEmpty = "";

			if (regexСloseTag.IsMatch(pTagsList[index].Value))
			{
				var lineP = pTagsList[index].Value;
				pTagsList[index].Value = regexСloseTag.Replace(lineP, targetEmpty);
				listHTMLComponents.Add(htmlComponent2);
				return;
			}
			for (; index < pTagsList.Count; index++)
			{
				if (regexOpenTag.IsMatch(pTagsList[index].Value) && dictionaryOpenTagKeyCloseTagValue.Keys.Contains(regexOpenTag.Match(pTagsList[index].Value).ToString()))
				{
					htmlComponent = new HTMLComponent() { Name = regexOpenTag.Match(pTagsList[index].Value).ToString() };
					++index;

					ParserWordToHTMLRecursion(ref index, pTagsList, htmlComponent, listHTMLComponents, dictionaryOpenTagKeyCloseTagValue);

					htmlComponent2.Value += listHTMLComponents[listHTMLComponents.Count - 1].Value;
				}
				// Условие в принципе ненужное, но на всякий случай(который не отловил).
				if (index >= pTagsList.Count)
					return;

				// Очищаем span от открывающихся тегов, записываем Name.
				// Здесь находим одиночные теги.
				if (regexOpenTag.IsMatch(pTagsList[index].Value) && !dictionaryOpenTagKeyCloseTagValue.Keys.Contains(regexOpenTag.Match(pTagsList[index].Value).ToString()))
				{
					var spanTags = pTagsList[index].Elements().ToList();
					HTMLComponent htmlComponent1 = new HTMLComponent();

					for (int spanIndex = 0; spanIndex < spanTags.Count; spanIndex++) // Отсюда берём открывающиеся теги и value 
					{
						var lineSpan = spanTags[spanIndex].Value;

						if (regexOpenTag.IsMatch(lineSpan) && !regexСloseTag.IsMatch(lineSpan))
						{
							htmlComponent1 = new HTMLComponent() { Name = regexOpenTag.Match(lineSpan).ToString() };
							spanTags[spanIndex].Value = regexOpenTag.Replace(lineSpan, targetEmpty);// Убираем из текста теги.
							listHTMLComponents.Add(htmlComponent1);
						}
						if (spanIndex == spanTags.Count - 1)
							htmlComponent1.Value = pTagsList[index].ToString();
					}
				}

				if (regexСloseTag.IsMatch(pTagsList[index].Value))
				{
					// Очищаем узел.
					var lineP = pTagsList[index].Value;
					pTagsList[index].Value = regexСloseTag.Replace(lineP, targetEmpty);
					listHTMLComponents.Add(htmlComponent2);
					return;
				}
				if (!regexOpenTag.IsMatch(pTagsList[index].Value))
					htmlComponent2.Value += pTagsList[index].ToString();
			}

		}

		static Dictionary<string, string> ParserLineToDictionaryTag(string word)
		{
			var dictionaryTag = new Dictionary<string, string>();
			//Regex regexOpenTag = new Regex(@"<[^/]\S*>");
			//Regex regexСloseTag = new Regex(@"</\S*>");
			var openTags = regexOpenTag.Matches(word);
			var СloseTags = regexСloseTag.Matches(word);
			foreach (var tag in openTags)
			{
				var tag1 = tag.ToString().Substring(1);
				foreach (var closeTag in СloseTags)
				{
					var tag2 = closeTag.ToString().Substring(2);
					if (tag1 == tag2)
						dictionaryTag[tag.ToString()] = closeTag.ToString();
				}
			}

			return dictionaryTag;
		}
	}
	public class HTMLComponent
	{
		public string Name { get; set; }
		public string Value { get; set; }
	}
	#endregion
}
