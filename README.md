# README #

Веб - приложение разработано для преобразования текста из word документа в текст c HTML разметкой с сохранением всего форматирования word документа, структура которого представляет определённый язык гипертекстовой разметки, результат работы приложения список объектов, которые содержат в себе тег и текст c HTML разметкой,  соответствующего тега, со всем форматированием из word документа. Пример текста word документа ниже.

<t>Offline OST to PST file conversion. OST to PST converter software.
<d>Microsoft Outlook OSTPST Converter Tool for 
Microsoft Exchange Server and Microsoft Outlook databases. The Offline OST to PST File Converter allows you to convert OST files to PST without having to connect to an Exchange Server or a Domain Controller.
<k>ost to pst, ost2pst converter, offline ost to pst converter, ost to pst offline converter, ost to pst file conversion, ost ->pst, ostpst conversion tool, osttopst converter
<h1>Offline OST to PST file converter
<text1>
The OST to PST file converter can be installed on computers running Windows with Microsoft Outlook installed. You do not need to be connected to the Internet.
<t>Offline OST to PST file conversion. OST to PST converter software.
<h2-il>
•	You do not need to upload files to a web server
•	You do not need to sign a non-disclosure agreement (NDA)
•	There are no restrictions on the size of the OST file
•	There are no restrictions on the number of OST files
</h2-il>
<text3>
The ostpst conversion tool allows you to:
•	Convert damaged OST files
•	Convert OST files after an unsuccessful attempt to recover them using the program ScanOST.exe
•	Convert OST files when the Mail Profile is missing, damaged, or replaced
</text3>
The tool lets you convert Microsoft Exchange Server and Microsoft Outlook databases (*.OST files) into Microsoft Outlook Personal Store Folders (*.PST files) on personal computers or servers that have Microsoft Outlook installed. The osttopst converter works without an Internet connection.
</text1>
<h2>Benefits of using the Offline OST to PST Converter:
<h2-il>
•	You do not need to upload files to a web server
•	You do not need to sign a non-disclosure agreement (NDA)
•	There are no restrictions on the size of the OST file
•	There are no restrictions on the number of OST files
</h2-il>