﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DocumentFormat.OpenXml.Packaging;
using OpenXmlPowerTools;
using System.IO;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using ParserWordToHTML.BLL.Services;

namespace ParserWordToHTML.Controllers
{
	public class ValuesController : ApiController
	{
		//POST api/values
		public async Task<List<HTMLComponent>> Post()
		{

			var provider = new MultipartMemoryStreamProvider();
			await Request.Content.ReadAsMultipartAsync(provider);

			var listHTMLComponents = new List<HTMLComponent>();
			foreach (var file in provider.Contents)
			{
				byte[] fileArray = await file.ReadAsByteArrayAsync();
				listHTMLComponents = ParserWordToHTMLService.CreateListObjectHTMLComponent(fileArray);
			}
			return listHTMLComponents;
		}


		// GET api/values
		public IEnumerable<string> Get()
		{
			return new string[] { "value1", "value2" };
		}

		// GET api/values/5
		public string Get(int id)
		{
			return "value";
		}
		// PUT api/values/5
		public void Put(int id, [FromBody]string value)
		{
		}

		// DELETE api/values/5
		public void Delete(int id)
		{
		}
	}
}
