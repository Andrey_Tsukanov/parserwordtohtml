﻿angular.module("exampleApp", [])
.constant("baseUrl", "http://localhost:54410/api/values/post")
.controller("defaultCtrl", function ($scope, $http, $sce, baseUrl) {

    // Текущее педставление
    $scope.currentView = "loading";


    $scope.download = function () {
        // Отправка POST запроса для получения данных.
        var files = document.getElementById('uploadFile').files;
        if (files.length > 0) {
            if (window.FormData !== undefined) {
                var data = new FormData();
                for (var x = 0; x < files.length; x++) {
                    data.append("file" + x, files[x]);
                }
                //$http({
                //    url: 'http://localhost:54410/api/values/post',
                //    method: "POST",
                //    data: data
                //}).success(function (modifiedItem) {
                //    //$scope.items = modifiedItem;
                //    $scope.currentView = "table";
                //    alert(modifiedItem);
                //});
               
                $.ajax({
                    type: "POST",
                    url: baseUrl,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        $scope.items = result;                       
                    },
                    error: function (xhr, status, p3) {
                        alert(status);
                    }
                });
            }
        }
    }
    
    $scope.clear = function () {
        $scope.htmlcode = "";
    }
    $scope.newDownload = function () {
        $scope.htmlcode = "";
        $scope.currentView = "loading";
    }
   
    $scope.display = function () {
        
        $scope.currentView = "table";
    }
    $scope.htmlcode = "";
    $scope.$watch("htmlcode", function (newValue) {
        $scope.htmlcode = $sce.trustAsHtml(newValue);
    });
});